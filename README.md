## Workspacex
Docker + Debian 10 + PHP7.3 + Phalcon 3 + SqLite3

#### Resources
- Platform [Docker](https://www.docker.com/)
- Specification [JSON-RPC 2.0](https://www.jsonrpc.org/specification)

#### Microservices launch
- Build images: `docker-compose up -d --build`

