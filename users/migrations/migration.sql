-- Set encoding for data
PRAGMA encoding="UTF-8"

-- Create a table for users
CREATE TABLE `users` (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	login CHAR(50) NOT NULL UNIQUE,
	password VARCHAR(255) NOT NULL
);

-- Add first user to user table
INSERT INTO `users` ( `login`, `password` ) VALUES ( 'admin', '$2y$08$MTEyOXl6R0phckJ0NHhrMeJsxSMACI0l3i/aQypisc7nchijI1Uu6' );
