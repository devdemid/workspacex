<?php

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Dispatcher configuration
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

/**
 * Common config
 */
$di->setShared( 'config', $config );

/**
 * Mvc\Dispatcher
 */
$di->setShared( 'dispatcher', '\App\Core\Mvc\Dispatcher' );

/**
 * Http\Request
 */
$di->setShared( 'request', '\Phalcon\Http\Request' );

/**
 * Http\Response
 */
$di->setShared( 'response', '\Phalcon\Http\Response' );

/**
 * JsonRPC\Authorization
 */
$di->setShared( 'auth', '\App\Core\Http\Request\Auth' );

/**
 * Security
 */
$di->setShared( 'security', '\Phalcon\Security' );

/**
 * JsonRPC\Router
 */
$di->setShared( 'router', '\App\Core\Http\JsonRPC\Router' );

/**
 * JsonRPC\Response
 */
$di->setShared( 'jsonrpcResponse', function() use( $config ) {
	$response = new \App\Core\Http\JsonRPC\Response;
	// Overriding Response-object to set the Content-type header globally
	$response->setContentType( 'application/json', $config->path( 'api.charset', 'utf-8' ) );
	return $response;
});

/**
 * JsonRPC\Request
 */
$di->setShared( 'jsonrpcRequest', function() {
	$request = \Phalcon\DI::getDefault()->getShared( 'request' );
	return \App\Core\Http\JsonRPC\Request::init( (string) $request->getRawBody() );
} );

/**
 * Database
 */
$di->set( 'db', function() use ( $config ) {
	$settings = $config->get( 'db', [] )->toArray();
	return new \Phalcon\Db\Adapter\Pdo\SQLite( $settings );
});
