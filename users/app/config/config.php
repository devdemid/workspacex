<?php

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * General config
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

return new \Phalcon\Config([
	'db' => [
		'dbname' => ROOT_PATH . '/db/database.sqlite',
	],
	'api' => [
		'charset' => 'utf-8',
		// Unique signature verification string
		'authorization_secret' => '$w^Rх✇фʎ₢q-ц%✶$£Px3⋆$✶ҺK$Gs♜',
	],
]);
