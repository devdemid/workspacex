<?php

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Loader
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

/**
 * We're a registering a set of directories taken from the configuration file
 */
( new \Phalcon\Loader )
	->registerNamespaces([
		// Core
		'App\Core' => APP_PATH . '/core',
		'App\Core\Http' => APP_PATH . '/core/http',
		'App\Core\Http\Api' => APP_PATH . '/core/http/api',
		'App\Core\Http\JsonRPC' => APP_PATH . '/core/http/jsonrpc',
		'App\Core\Http\Request' => APP_PATH . '/core/http/request',
		'App\Core\Mvc' => APP_PATH . '/core/mvc',
		// Api v1
		'App\Api' => APP_PATH . '/api',
		'App\Api\v1\Controller' => APP_PATH . '/api/v1/controllers',
	])
	->register();
