<?php

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Temporary storage of keys for access to api
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

return [
	md5( 'site.workspacex.uniqueid' ) => md5( 'site.workspacex.key' )
];
