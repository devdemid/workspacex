<?php

namespace App\Core\Http\JsonRPC;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Router class for JsonRPC 2.0
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 *
 * @link https://www.jsonrpc.org/specification
 */

class Router extends \Phalcon\Mvc\Router
{
	/**
	 * Controller and Method Separator
	 * @var string
	 */
	const SEPARATOR_FOR_METHOD  = '.';

	/**
	* Handles request
	* @param string $data
	* @return void
	*/
	public function handle( $data = NULL )
	{
		/**
		 * Get JsonRPC request
		 * @var App\Core\Http\JsonRPC\Request $request
		 */
		if ( $data ) {
			$request = Request::init( $data );
		} else {
			$request = $this->getDI()->getShared( 'jsonrpcRequest' );
		}

		/**
		 * Parse method name `controller.action`
		 * @var array
		 */
		if ( strstr( $request->method, self::SEPARATOR_FOR_METHOD ) === FALSE ) {
			$controller = mb_strtolower( $request->method );
		} else {
			list( $controller, $action ) = explode( self::SEPARATOR_FOR_METHOD, $request->method, 2 );
		}

		/**
		 * Request controller
		 * @var string
		 */
		$controller = isset( $controller )
			? mb_strtolower( $controller )
			: 'main'; // Default controller

		/**
		 * Request action
		 * @var string
		 */
		$action = isset( $action )
			? mb_strtolower( $action )
			: 'index'; // Default action

		/**
		 * Setup variables and run dispatcher
		 */
		$dispatcher = $this->getDi()->getShared( 'dispatcher' );
		// TODO: Auto detected a namespace
		$dispatcher->setNamespaceName( '\App\Api\v1\Controller' );
		$dispatcher->setControllerName( $controller );
		$dispatcher->setActionName( $action );
		$dispatcher->setParams( [ $request->params->toArray() ] );
		$dispatcher->dispatch();
	}
}
