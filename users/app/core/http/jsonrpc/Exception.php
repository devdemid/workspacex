<?php

namespace App\Core\Http\JsonRPC;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Exception for JsonRPC 2.0
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

class Exception extends \Exception
{
	/**
	 * Internal error
	 * @var integer
	 */
	const ERROR_INTERNAL_ERROR = -1000;

	/**
	 * Bad Request
	 * @var integer
	 */
	const ERROR_BAD_REQUEST = -1001;

	/**
	 * Parse error
	 * @var integer
	 */
	const ERROR_PARSE_ERROR = -1002;

	/**
	 * Invalid Request
	 * @var integer
	 */
	const ERROR_INVALID_REQUEST = 1003;

	/**
	 * Method not found
	 * @var integer
	 */
	const ERROR_METHOD_NOT_FOUND = -1004;

	/**
	 * Invalid params
	 * @var integer
	 */
	const ERROR_INVALID_PARAMS = -1005;

	/**
	 * Invalid identifier
	 * @var integer
	 */
	const ERROR_INVALID_IDENTIFIER = -1006;
}
