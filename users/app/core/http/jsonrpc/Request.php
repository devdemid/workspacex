<?php

namespace App\Core\Http\JsonRPC;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Request class for JsonRPC 2.0
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 *
 * @link https://www.jsonrpc.org/specification
 */

//use \App\Core\Http\Exception AS Exception;

class Request extends \Phalcon\Http\Request
{
	/**
	 * Supported JsonRPC Version
	 * @var string
	 */
	const VERSION = '2.0';

	/**
	 * Request id
	 * @var string|int|null
	 */
	public $id;

	/**
	 * Request version
	 * @var string
	 */
	public $version;

	/**
	 * Method
	 * @var string
	 */
	public $method;

	/**
	 * Parameters through a config
	 * @var \Phalcon\Config
	 */
	public $params;

	/**
	 * Creates request object from string
	 * @param string $json The request body
	 * @return self
	 */
	public static function init( string $json = '' )
	{
		/**
		 * Check that request is not empty
		 */
		if ( empty( $json ) ) {
			throw new Exception( _( 'Request body is empty' ), Exception::ERROR_INVALID_REQUEST );
		}

		/**
		 * Decode given json
		 */
		$data = json_decode( $json, TRUE );

		/**
		 * If there is parse error, throw exception
		 */
		if ( json_last_error() !== JSON_ERROR_NONE ) {
			switch ( json_last_error() ) {
				case JSON_ERROR_DEPTH:
					$message = _( 'Maximum stack depth exceeded' );
					break;
				case JSON_ERROR_STATE_MISMATCH:
					$message = _( 'Underflow or the modes mismatch' );
					break;
				case JSON_ERROR_CTRL_CHAR:
					$message = _( 'Unexpected control character found' );
					break;
				case JSON_ERROR_SYNTAX:
					$message = _( 'Syntax error, malformed JSON' );
					break;
				case JSON_ERROR_UTF8:
					$message =_( 'Malformed UTF-8 characters, possibly incorrectly encoded' );
					break;
				default:
					$message = _( 'Unknown parsing error' );
					break;
			}

			throw new Exception( $message, Exception::ERROR_PARSE_ERROR );
		}

		/**
		 * Set up version
		 */
		if ( empty( $data['jsonrpc'] ) OR $data['jsonrpc'] !== self::VERSION ) {
			throw new Exception( _( 'Incorrect JSON-RPC version' ), Exception::ERROR_INVALID_REQUEST );
		}

		/**
		 * If there is no ID, throw exception
		 */
		if ( empty( $data['id'] ) ) {
			throw new Exception( _( 'ID is incorrect' ), Exception::ERROR_INVALID_REQUEST );
		}

		/**
		 * If there is no method, throw exception
		 */
		if ( empty( $data['method'] ) ) {
			throw new Exception( _( 'Method can not be empty' ), Exception::ERROR_METHOD_NOT_FOUND );
		}

		/**
		 * If threre is no params, throw exception
		 */
		if ( ! isset( $data['params'] ) ) {
			throw new Exception( _( 'Params are not specified' ), Exception::ERROR_INVALID_PARAMS );
		}

		/**
		 * Create and fill in jsonrpc request
		 */
		$request = new self;
		$request->id      = $data['id'];
		$request->method  = (string) $data['method'];
		$request->params = new \Phalcon\Config( (array) $data['params'] );
		$request->version = (string) $data['jsonrpc'];

		return $request;
	}
}
