<?php

namespace App\Core\Http\JsonRPC;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Response class for JsonRPC 2.0
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 *
 * @link https://www.jsonrpc.org/specification
 */

class Response extends \Phalcon\Http\Response
{
	/**
	 * Request id
	 * @access protected
	 * @var string|int|null
	 */
	protected $id;

	/**
	 * Method execution result
	 * @access protected
	 * @var string
	 */
	protected $result;

	/**
	 * Error occured while executing
	 * JsonRPC request
	 * @access private
	 * @var Exception $error
	 */
	private $error = [];

	/**
	 * Set the request identifier.
	 * @param string|int $id The request identifier
	 * @return self
	 */
	public function setId( $id = NULL )
	{
		$this->id = ( is_integer( $id ) OR is_null( $id ) )
			? $id
			: (string) $id;
		return $this;
	}

	/**
	 * Set the error content.
	 * @param string $message Error message
	 * @param integer $code Error code
	 * @param string $documentation_url Link to error documentation
	 * @return self
	 */
	public function setErrorContent( string $message = '', int $code = 0, string $documentation_url = NULL )
	{
		$this->error = [
			'code'    => (int) $code,
			'message' => (string) $message,
		];

		/**
		 * Link to error documentation
		 */
		if ( ! empty( $documentation_url ) ) {
			$this->error['documentation_url'] = $documentation_url;
		}

		return $this;
	}

	/**
	 * Return string representation
	 * @return string
	 */
	public function getContent(): string
	{
		$response = [
			'id'      => $this->id,
			'jsonrpc' => (string) Request::VERSION,
		];

		/**
		 * Use the current content
		 * @var string
		 */
		$result = parent::getContent();

		/**
		 * Decode json string to PHP object or array for output of `$this->jsonrpcResponse->setJsonContent( array... )`
		 */
		if ( $json_result = json_decode( $result, TRUE ) ) {
			$result = $json_result;
		}

		if ( ! empty( $this->error ) ) {
			$response[ 'error' ] = (array) $this->error;
		} else {
			$response[ 'result' ] = $result;
		}

		return json_encode( $response );
	}

	/**
	 * Sending data to a client
	 * @return void
	 */
	public function send()
	{
		$this->_content = $this->getContent();
		parent::send();
	}
}
