<?php

namespace App\Core\Http\Request;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Request Authorization
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

class Auth extends \Phalcon\Mvc\User\Component {

	/**
	 * Key and Signature Separator
	 * @var string
	 */
	const SEPARATOR = ':';

	/**
	 * Request Authorization Check
	 * @return bool
	 */
	public function checkAuthorization(): bool
	{
		$confirmation = TRUE;
		$auth_header = $this->request->getHeader( 'Auth' );

		// Primitive signature verification for demonstration
		if ( ! empty( $auth_header ) AND strstr( $auth_header, self::SEPARATOR ) !== FALSE ) {
			list( $client_uniqueid, $key ) = explode( self::SEPARATOR, $auth_header, 2 );

			$auth_access_keys = require_once APP_PATH . '/config/auth_access_keys.php';
			if (
				! $client_uniqueid
				OR ! $key
				OR ! isset( $auth_access_keys[ $client_uniqueid ] )
				OR ! $auth_access_keys[ $client_uniqueid ]
			) {
				$confirmation = FALSE;
			}

			if ( $key !== md5( $auth_access_keys[ $client_uniqueid ] . $client_uniqueid ) ) {
				$confirmation = FALSE;
			}
		} else {
			$confirmation = FALSE;
		}

		if ( $confirmation === FALSE ) {
			throw new \Phalcon\Http\Request\Exception( _( 'Requires authentication' ), 401 );
		}

		return $confirmation;
	}
}
