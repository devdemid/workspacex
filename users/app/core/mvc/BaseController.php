<?php

namespace App\Core\Mvc;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Controller base class
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

class BaseController extends \Phalcon\Mvc\Controller
{
}
