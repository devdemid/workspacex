<?php

namespace App\Core\Mvc;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * MVC Dispatcher
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

class Dispatcher extends \Phalcon\Mvc\Dispatcher
{
	/**
	 * @return self
	 */
	public function dispatch()
	{
		parent::dispatch();
		if( $result = $this->getReturnedValue() ) {
			$di = $this->getDi();
			$jsonrpcResponse = $di->getShared( 'jsonrpcResponse' );
			$jsonrpcResponse->setId( $di->getShared( 'jsonrpcRequest' )->id );
			if ( is_array( $result ) ) {
				$jsonrpcResponse->setJsonContent( $result );
			} elseif ( is_string( $result ) ) {
				$jsonrpcResponse->setContent( $result );
			}
			$jsonrpcResponse->send();
			exit;
		}
		return $this;
	}
}
