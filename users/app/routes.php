<?php

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * All routes
 * @copyright Copyright (c) 2019 Workspacex
 */

/**
 * Not found
 */
$app->notFound( function () use ( $app ) {
	throw new \Phalcon\Http\Response\Exception( 'Not Found', 404 );
});
