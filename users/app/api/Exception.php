<?php

namespace App\Api;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * General Exception
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

class Exception extends \Exception
{
	/**
	 * Login not set
	 * @var integer
	 */
	const ERROR_AUTH_LOGIN_NOT_SET = 4000;

	/**
	 * Password not set
	 * @var integer
	 */
	const ERROR_AUTH_PASSWORD_NOT_SET = 4001;

	/**
	 * No database entry
	 * @var integer
	 */
	const ERROR_AUTH_NO_DB_ENTRY = 4002;

	/**
	 * Invalid password
	 * @var integer
	 */
	const ERROR_AUTH_INVALID_PASSWORD = 4003;
}
