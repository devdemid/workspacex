<?php

namespace App\Api\v1\Controller;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Class for user authorization
 * @copyright Copyright (c) 2019 Workspacex
 */

use \Phalcon\Db AS DB;
use \App\Api\Exception AS ApiException;

class AuthController extends \App\Core\Mvc\BaseController {

	/**
	 * Login and password verification
	 * @param array $params JsonRPC 2.0 Params
	 * @throws \App\Core\Http\Api\Exception
	 * @return mixed|string|array
	 */
	public function loginAction( array $params = [] )
	{
		if ( ! $login = $this->jsonrpcRequest->params->get( 'login' ) ) {
			throw new ApiException( _( 'Login not set' ), ApiException::ERROR_AUTH_LOGIN_NOT_SET );
		}
		if ( ! $password = $this->jsonrpcRequest->params->get( 'password' ) ) {
			throw new ApiException( _( 'Password not set' ), ApiException::ERROR_AUTH_PASSWORD_NOT_SET );
		}

		$db = $this->db;
		$user = $db->fetchOne("SELECT * FROM `users` WHERE `login` = :login", DB::FETCH_OBJ, [
			'login' => $login
		]);

		if ( ! $user ) {
			throw new ApiException( _( 'No database entry' ), ApiException::ERROR_AUTH_NO_DB_ENTRY );
		}
		elseif ( ! $this->security->checkHash( $password, $user->password ) ) {
			throw new ApiException( _( 'Invalid password' ), ApiException::ERROR_AUTH_INVALID_PASSWORD );
			return 'Неверный пароль';
		}
		else {
			return [
				'id' => $user->id,
				'login' => $user->login,
			];
		}

		return '';
	}

	/**
	 * Logout action for test
	 * @return string
	 */
	public function logoutAction() {
		return 'logout';
	}
}
