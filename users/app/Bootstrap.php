<?php

namespace App;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Bootstrap
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

class Bootstrap
{
	/**
	 * Run the installed module name
	 * @param string $name Unique app name
	 */
	public function run( string $name = NULL )
	{
		try
		{
			/**
			 * Loading Config
			 * @var array
			 */
			$config = require_once APP_PATH . '/config/config.php';

			/**
			 * Autoloading classes
			 */
			require_once APP_PATH . '/config/loader.php';

			/**
			 * Dependency Injector
			 * @var Phalcon\Di $di
			 */
			$di = new \Phalcon\Di;
			require_once APP_PATH . '/config/di.php';

			/**
			 * Initializing application
			 * @var Phalcon\Mvc\Micro $app
			 */
			$app = new \Phalcon\Mvc\Micro;
			$app->setDI( $di );

			/**
			 * Check Authorization
			 * @var \Phalcon\Events\Manager $eventsManager
			 */
			$eventsManager = new \Phalcon\Events\Manager;
			$eventsManager->attach( 'micro:beforeHandleRoute', function ( \Phalcon\Events\Event $event, $app ) {
				return $app->getDi()
					->getShared( 'auth' )
					->checkAuthorization();
			});
			$app->setEventsManager( $eventsManager );

			/**
			 * Setting up routing
			 */
			require_once APP_PATH . '/routes.php';

			/**
			 * Processing request
			 */
			$app->handle();
		}
		catch ( \PDOException $e )
		{
			if( defined( 'APP_DEBUG' ) AND APP_DEBUG ) {
				var_export( $e );
			}
			exit;
		}
		catch ( \Exception $e )
		{
			$code = $status_code = $e->getCode();
			$status_message = $e->getMessage();

			/**
			 * Internal codes retrieved as 400 Bad Request
			 */
			if ( $code <= 199 ) {
				$status_code = 400;
				$status_message = 'Bad Request';
				// TODO: Logging errors to the log file
			}

			/**
			 * Get request id
			 */
			try {
				$jsonrpc_id = $app->jsonrpcRequest->id;
			} catch ( \Exception $err  ) {
				$jsonrpc_id = NULL;
			}

			echo $app->jsonrpcResponse
				->setHeader( 'application/json', $app->config->path( 'api.charset', 'utf-8' ) )
				->sendHeaders()
				->setStatusCode( $status_code, $status_message )
				->setId( $jsonrpc_id )
				->setErrorContent( $e->getMessage(), $code )
				->getContent();
			exit;
		}
	}
}
