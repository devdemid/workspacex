<?php

namespace App;

/**
 * Main Application
 * @copyright Copyright (c) 2019 Workspacex
 */

/**
 * Directories
 */
defined( 'WEB_PATH' )  OR define( 'WEB_PATH', __DIR__ );
defined( 'ROOT_PATH' ) OR define( 'ROOT_PATH', realpath( WEB_PATH . '/..' ) );
defined( 'APP_PATH' )  OR define( 'APP_PATH', realpath( ROOT_PATH . '/app' ) );

/**
 * Connecting common configurations
 */
require_once APP_PATH . '/config/bootstrap.php';

/**
 * Checking enable the phalcon extension
 */
if ( ! extension_loaded( 'phalcon' ) ) {
	exit( 'Could not load extension phalcon must be included in the php.ini file.' );
}

/**
 * Initializing the application
 */
require APP_PATH . '/Bootstrap.php';
( new \App\Bootstrap )->run( 'site' );
exit;
