<?php

namespace App\Service;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Users api
 * @copyright Copyright (c) 2019 Workspacex
 */

class Api extends \Phalcon\Mvc\User\Component
{
	/**
	 * Auth separator
	 * @var string
	 */
	const SEPARATOR = ':';

	/**
	 * Request id
	 * @var string|int
	 */
	public $id;

	/**
	 * Api endpoint
	 * @var string
	 */
	private $endpoint;

	/**
	 * Signature for authorization
	 * @var string
	 */
	private $auth;

	/**
	 * Service initialization
	 * @param string  $endpoint The endpoint
	 * @param string  $client The client
	 * @param string  $secret_key The secret key
	 * @return void
	 */
	public function __construct( string $endpoint, string $client = '', string $secret_key = '' )
	{
		$this->endpoint = $endpoint;
		$this->auth = $client . self::SEPARATOR . $secret_key;
	}

	/**
	 * Run request
	 * @param string $method
	 * @param array $params
	 * @return mixed
	 */
	public function request( string $id, string $method, array $params = [] )
	{
		/**
		 * JsonRPC 2.0 specification request format
		 * @link https://www.jsonrpc.org/specification
		 * @var array
		 */
		$jsonrpcRequest = [
			'id' => $id,
			'jsonrpc' => '2.0',
			'method' => $method,
			'params' => $params,
		];

		$response = $this->curl( $jsonrpcRequest);
		if ( ! empty( $response['code'] ) AND $response['body'] !== FALSE ) {
			$response['body'] = json_decode( $response['body'], TRUE );

			/**
			 * If there is parse error, throw exception
			 */
			if ( json_last_error() !== JSON_ERROR_NONE ) {
				switch ( json_last_error() ) {
					case JSON_ERROR_DEPTH:
						$message = _( 'Maximum stack depth exceeded' );
						break;
					case JSON_ERROR_STATE_MISMATCH:
						$message = _( 'Underflow or the modes mismatch' );
						break;
					case JSON_ERROR_CTRL_CHAR:
						$message = _( 'Unexpected control character found' );
						break;
					case JSON_ERROR_SYNTAX:
						$message = _( 'Syntax error, malformed JSON' );
						break;
					case JSON_ERROR_UTF8:
						$message =_( 'Malformed UTF-8 characters, possibly incorrectly encoded' );
						break;
					default:
						$message = _( 'Unknown parsing error' );
						break;
				}
				$response['error'] = $message;
			}
		}
		return $response;
	}

	/**
	 * Run a server request through CURL
	 * @param array $jsonrpcRequest
	 * @return array
	 */
	private function curl( array $jsonrpcRequest = [] ): array
	{
		/**
		 * Convert string to json string for query
		 */
		$jsonrpcRequest = json_encode( $jsonrpcRequest );

		/**
		 * Set HTTP Header for POST request
		 * @link https://en.wikipedia.org/wiki/List_of_HTTP_header_fields
		 */
		$headers = [
			'Content-Type: application/json',
			'Content-Length: ' . strlen( $jsonrpcRequest ),
			'Auth: ' . $this->auth,
		];

		/**
		 * Prepare new cURL resource
		 * @link https://www.php.net/manual/ru/function.curl-setopt.php
		 */
		$options = [
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLINFO_HEADER_OUT    => TRUE,
			CURLOPT_POST           => TRUE,
			CURLOPT_POSTFIELDS     => TRUE,
			CURLOPT_CONNECTTIMEOUT => 0,
			CURLOPT_TIMEOUT        => 200,
			CURLOPT_POSTFIELDS     => $jsonrpcRequest,
			CURLOPT_HTTPHEADER     => $headers,
		];

		$ch = curl_init( $this->endpoint );
		curl_setopt_array( $ch, $options );
		$body = curl_exec( $ch );
		$http_code = curl_getinfo( $ch, CURLINFO_RESPONSE_CODE );
		curl_close( $ch );

		return [
			'code' => $http_code,
			'body' => $body,
		];
	}
}
