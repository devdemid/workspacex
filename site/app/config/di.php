<?php

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Dispatcher configuration
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

use \Phalcon\DI\FactoryDefault;
use \Phalcon\Mvc\View\Simple as View;
use \Phalcon\Mvc\Url as UrlResolver;
use \Phalcon\Mvc\View\Engine\Volt as VoltEngine;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new \Phalcon\DI\FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () use ( $config ) {
	$url = new UrlResolver();
	$url->setBaseUri( $config->app->baseUri );
	return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () use ( $config ) {
	$view = new View();
	$view->setViewsDir( $config->app->viewsDir );
	$view->registerEngines(
		[
			'.php' => '\Phalcon\Mvc\View\Engine\Php'
		]
	);
	return $view;
});

/**
 * JsonRPC\Api
 */
$di->setShared( 'api', function() use ( $config ) {
	$api_config = $config->get( 'api' )->toArray();
	$api = new \App\Service\Api( $api_config[ 'host' ], $api_config[ 'client' ], $api_config[ 'secret_key' ] );
	return $api;
} );
