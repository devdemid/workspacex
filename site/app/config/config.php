<?php

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * General config
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

return new \Phalcon\Config([
	'app' => [
		'viewsDir' => APP_PATH . '/views/',
		'baseUri'  => '/',
	],
	'api' => [
		'host' => 'http://workspacex_users_1',
		// Temporary data for demonstration
		'client' => 'cae2b62ec0bb3629f92035710455ff37',
		'secret_key' => '5da1c696b5693a4eff7a303019316d2f',
	],
]);
