<?php

/**
 * Connecting common configurations
 * @copyright Copyright (c) 2019 Workspacex
 * @since 1.0
 */

error_reporting( E_ALL );

@ini_set( 'display_errors', TRUE );
@ini_set( 'html_errors', FALSE );

/**
 * Global settings
 */
defined( 'ABSPATH'   ) OR define( 'ABSPATH', TRUE );
defined( 'APP_DEBUG' ) OR define( 'APP_DEBUG', FALSE );
defined( 'APP_DEV'   ) OR define( 'APP_DEV', FALSE );

