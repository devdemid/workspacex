<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Авторизация в системе</title>
		<!-- Bootstrap core CSS -->
		<link href="/assets/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="/assets/css/signin.css" rel="stylesheet">
	</head>
	<body class="text-center">
		<!-- Begin signin form-->
		<form action="/index" method="post" class="form-signin">
			<?php if( isset( $message ) AND ! empty( $message ) ): ?>
			<div class="alert <?php echo $alert ?>" role="alert">
				<?php echo $message ?>
			</div>
			<?php endif ?>
			<h1 class="h3 mb-3 font-weight-normal">Авторизация в системе</h1>
			<label for="login" class="sr-only">Ваш логин:</label>
			<input type="text" id="login" name="login" class="form-control" placeholder="Ваш логин" required autofocus>
			<label for="password" class="sr-only">Ваш пароль:</label>
			<input type="password" id="password" name="password" class="form-control" placeholder="Ваш пароль" required>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
			<div class="mt-3 text-muted">
				Логин: <code>admin</code> Пароль: <code>admin</code>
			</div>
			<p class="mb-3 text-muted">2019 &copy; Все права защищены!</p>
		</form>
		<!-- End signin form -->
	</body>
</html>

