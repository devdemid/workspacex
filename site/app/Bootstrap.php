<?php

namespace App;

defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Bootstrap
 * @copyright Copyright (c) 2019 Workspacex
 */

use \Phalcon\DI\FactoryDefault;
use \Phalcon\Loader;
use \Phalcon\Mvc\Dispatcher;
use \Phalcon\Mvc\Micro;
use \Phalcon\Mvc\Micro\Collection as MicroCollection;


class Bootstrap
{
	/**
	 * Run the installed module name
	 * @param string $name Unique app name
	 */
	public function run( string $name = NULL )
	{
		try
		{
			/**
			 * Loading Config
			 * @var array
			 */
			$config = require_once APP_PATH . '/config/config.php';

			/**
			 * Include Autoloader
			 */
			( new Loader )->registerNamespaces([
				'App\Service' => APP_PATH . '/services',
			])->register();

			/**
			 * Dependency Injector
			 * @var \Phalcon\DI\FactoryDefault $di
			 */
			$di = new FactoryDefault;
			require_once APP_PATH . '/config/di.php';

			/**
			 * Initializing application
			 * @var Phalcon\Mvc\Micro $app
			 */
			$app = new Micro;
			$app->setDI( $di );

			/**
			 * Routes
			 */
			$app->get('/', function () use ( $app ) {
				echo $app->view->render('index');
			});

			$app->post('/', function () use ( $app ) {
				$params = $app->request->getPost();
				$params = array_map( 'trim', $params );

				$res = $app->api
					->request( microtime( TRUE ), 'auth.login', $params );

				if ( $res['code'] === 200 AND $res['body'] !== FALSE)
				{
					$body = $res['body'];
					if ( isset( $body['result']['id'] ) ) {
						$vars = [
							'message' => 'Авторизация успешная',
							'alert' => 'alert-success',
						];
					}
					else {
						$vars = [
							'message' => 'Не удалось авторизироваться',
							'alert' => 'alert-danger'
						];
					}
				}

				echo $app->view
					->setVars( $vars )
					->render( 'index' );
			});

			$app->notFound(function () use ( $app ) {
				echo $app->view->render( '404' );
			});

			$app->error(function () use ( $app ) {
				echo $app->view->render( '500' );
			});

			/**
			 * Processing request
			 */
			$app->handle();
		}
		catch ( \Exception $e )
		{
			var_export($e); exit;
		}
	}
}
